module.exports = {
  deleteImageById: require('./deleteImageById'),
  getImageById: require('./getImageById'),
  getImages: require('./getImages'),
  greyscaleImage: require('./greyscaleImage'),
  invertImage: require('./invertImage'),
  resizeImage: require('./resizeImage'),
  rotateImage: require('./rotateImage'),
  uploadImage: require('./uploadImage'),
  viewImage: require('./viewImage')
};
