module.exports = {
  deleteImageById: require('./deleteImageById'),
  getImage: require('./getImage'),
  getImageById: require('./getImageById'),
  getImages: require('./getImages'),
  updateImage: require('./updateImage'),
  uploadImage: require('./uploadImage')
};
