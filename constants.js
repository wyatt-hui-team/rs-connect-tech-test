module.exports = {
  imageMimeType: [
    'image/png',
    'image/gif',
    'image/jpeg',
    'image/bmp',
    'image/tiff'
  ]
};
